const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const birds = require('./routes/birds');
const user = require('./routes/user.route');
const home = require('./routes/home.route');
const auth = require('./services/auth.service');
const socketIo = require('socket.io')
const http = require('http');
// const https = require('https');
const fs = require('fs');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const createServer = () => {
    const app = express();
    const specs = swaggerJsDoc({
        definition: {
            openapi: "3.0.0",
            info: {
                title: 'Express',
                version: '1.0.0',
                description: 'Express application'
            },
            servers: [{
                url: 'http://localhost:3030'
            }]
        },
        apis: ["./routes/*.js"],
    });

    // config system
    app.set('view engine', 'pug');
    app.set('views', './views')

    // config middle ware
    app.use(cors());
    app.use(bodyParser.json());
    app.use(auth.initialize());

    // config router
    app.use('/', home);
    app.use('/birds', auth.authenticate(), birds);
    app.use('/user', auth.authenticate(), user);
    app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(specs));

    // fallback
    app.use((req, res, next) => {
        res.status(404)
        // console.log(req.accepts('json'));
        if (req.accepts('html')) {
            res.render('404', { url: req.url, title: "Page not found", message: 'Please try again' })
            return
        }
        if (req.accepts('json')) {
            res.json({ status: 'Error', message: 'Page not found.' });
            return
        }
        res.type('txt').send("page not found");
        return
    });

    const server = http.createServer(app);
    // const httpsOptions = {
    //     cert: fs.readFileSync('server.cert'),
    //     key: fs.readFileSync('server.key')
    // }
    // const server = https.createServer(httpsOptions, app);

    const io = socketIo(server);

    io.on('connection', (socket) => {
        socket.on('hello', (payload) => {
            console.log(payload);
            const { message } = payload;
            socket.emit('news', `data from server = ${message}`);
        });
        socket.on('message', (payload) => {
            io.emit('message', payload);
        });
    });

    return server;
}
module.exports = createServer;