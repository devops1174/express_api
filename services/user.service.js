const User = require('../repository/user');
const { redisClient } = require('../helpers/util');
class UserService {

    async findAll() {
        const cacheKey = 'user-findall';
        const ttl = 15;
        try {
            const cacheData = await redisClient.get(cacheKey);
            if (cacheData) {
                return JSON.parse(cacheData);
            }
            const resp = await User.findAll({ raw: true });
            await redisClient.setEx(cacheKey, ttl, JSON.stringify(resp));
            return resp;
        } catch (e) {
            throw new Error("find all error");
        }
    }

    async creatUser(name, email, password) {
        try {
            const resp = await User.create({ name, email, password });
            return resp;
        } catch (e) {
            throw new Error("can not create user");
        }
    }

    async delUser(id) {
        try {
            const resp = await User.destroy({
                where: { id }
            });
            return resp;
        } catch (e) {
            throw new Error("can not delete user");
        }
    }

    async updateUser(id, name, password) {
        try {
            const resp = await User.update({ name, password }, { where: { id } })
            return resp;
        } catch (e) {
            throw new Error("can not update user");
        }
    }

    async findByPage(offset, limit) {
        try {
            const resp = await User.findAll({ offset, limit, order: [['id', 'desc']] })
            const total = await User.count();
            return { resp, total };
        } catch (e) {
            throw new Error("can not get user by page");
        }
    }

    async findById(id) {
        try {
            const resp = await User.findByPk(id);
            return resp;
        } catch (e) {
            throw new Error("can not find user by id");
        }
    }

    async findByEmail(email) {
        try {
            const resp = await User.findOne({ where: { email } });
            return resp;
        } catch (e) {
            throw new Error("can not find user by id");
        }
    }
}

exports.userService = new UserService();