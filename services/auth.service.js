const passport = require('passport');
const passportJwt = require('passport-jwt');
const Strategy = passportJwt.Strategy;
const { secretKey } = require('../helpers/util');

class Auth {
    constructor() {
        let strategy = new Strategy({
            secretOrKey: secretKey,
            jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
        }, (payload, done) => {
            const user = payload;
            if (user) {
                return done(null, user);
            } else {
                return done(new Error("User not found !!!", null));
            }
        });
        passport.use(strategy);
    }

    initialize() {
        return passport.initialize();
    }

    authenticate() {
        return passport.authenticate('jwt', { session: false });
    }
}

module.exports = new Auth();