const { sequelize } = require('../helpers/util');
const { DataTypes } = require('sequelize');
const User = sequelize.define(
    "User",
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        name: {
            type: DataTypes.STRING(150),
            field: "user_name",
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING(50),
            field: "user_email",
            unique: true,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING(150),
            field: 'user_pwd',
            allowNull: false
        }
    },
    { tableName: "sc_user" }
);

module.exports = User;