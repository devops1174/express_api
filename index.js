const createServer = require("./server");
const { sequelize, redisClient } = require('./helpers/util');
const port = process.env.SERVER_PORT ?? 3030;
// const port = process.env.SERVER_PORT ?? 8443;
const User = require('./repository/user');

const app = createServer();
app.listen(port, async () => {
    try {
        const resp = await sequelize.sync();
        const redis = await redisClient.connect();
    } catch (e) {
        console.log(e.message);
    }
    console.log(`Server listen on ${port}`)
});