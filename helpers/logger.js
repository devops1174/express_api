const { createLogger, format, transports } = require('winston');

class Logger {
    constructor(name) {
        this.logger = createLogger({
            level: "debug",
            format: format.combine(
                format.colorize(),
                format.timestamp({
                    format: "YYYY-MM-DD HH:mm:ss",
                }),
                format.printf(
                    (info) => `${info.timestamp} ${info.level}: ${name}: ${info.message}`
                )
            ),
            transports: [
                new transports.Console({
                    level: "info",
                    format: format.combine(
                        format.colorize(),
                        format.printf(
                            (info) =>
                                `${info.timestamp} ${info.level} ${name}: ${info.message}`
                        )
                    ),
                }),
            ],
        });
    }

    info(str) {
        this.logger.info(str);
    }

    debug(str) {
        this.logger.debug(str);
    }
}

exports.Logger = Logger;