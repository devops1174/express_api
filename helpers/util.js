const { validationResult } = require('express-validator');
const { Sequelize } = require('sequelize');
const redis = require('redis');
const multer = require('multer');
const nodeMailer = require('nodemailer');

const inputValidate = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: errors });
    }
    next();
}


const sequelize = new Sequelize(
    process.env.DB_URL ?? 'postgres://usr:12345678@postgres:5432/pg_db'
);

const redisClient = redis.createClient({
    url: process.env.REDIS_URL ?? 'redis://redis:6379'
});

const secretKey = process.env.JWT_SECRET ?? 'super-secret-key';

const upload = multer({
    dest: 'uploads/'
});

const smtpTransport = nodeMailer.createTransport({
    host: "",
    port: 587,
    secure: false,
    auth: {
        user: '',
        pass: '',
    },
});

exports.inputValidate = inputValidate;
exports.sequelize = sequelize;
exports.redisClient = redisClient;
exports.secretKey = secretKey;
exports.upload = upload;
exports.smtpTransport = smtpTransport;