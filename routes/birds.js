const router = require('express').Router();
const { Logger } = require('../helpers/logger');
const { body, validationResult } = require('express-validator');
const { inputValidate } = require('../helpers/util');

const logger = new Logger('birds');

router.get('/', async (req, res) => {
    logger.info("get in birds route");
    res.json({ success: true, message: 'from get' });
});

const postValidator = [
    body('name').notEmpty().isLength({ min: 5, max: 20 }).withMessage("please input name value"),
    body('email').isEmail(),
    body('age').isInt({ min: 18, max: 99 }).withMessage('age must between 18 - 99'),
    inputValidate
];

router.post('/', ...postValidator,
    (req, res) => {
        const { name, email, age } = req.body;
        res.json({ success: true, message: 'from post', name, email, age });
    });

router.put('/', (req, res) => {
    res.json({ success: true, message: 'from put' });
});

router.delete('/', (req, res) => {
    res.json({ success: true, message: 'from delete' });
});

module.exports = router;