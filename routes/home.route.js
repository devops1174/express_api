const app = require('express').Router();
const { body } = require('express-validator');
const { inputValidate, secretKey } = require('../helpers/util');
const { userService } = require('../services/user.service');
const jwt = require('jwt-simple');

app.get('/', (req, res) => {
    res.render('index', { title: 'Hello', message: "Hello from pug" });
});

app.post('/', (req, res) => {
    res.status(401).json({ error: 'Error' });
});

app.get('/users/:userId', (req, res) => {
    const { userId } = req.params;
    res.json({ success: true, message: `Hello ${userId}` })
});

const cb1 = (req, res, next) => {
    console.log('in cb1');
    next();
}

const cb2 = (req, res) => {
    res.json({ success: true });
}

app.get('/example', [cb1, cb2]);

const loginValidator = [
    body('email').isEmail().notEmpty(),
    body('password').notEmpty(),
    inputValidate
]
app.post('/login', ...loginValidator, async (req, res) => {
    try {
        const { email, password } = req.body;
        const userInfo = await userService.findByEmail(email);
        if (!userInfo) {
            res.status(401).json({ message: 'Invalid user or password' });
        } else {
            const { password: pwd } = userInfo;
            if (password != pwd) {
                res.status(401).json({ message: 'Invalid user or password' });
            } else {
                const { email: sub, name } = userInfo;
                const token = jwt.encode({ sub, name }, secretKey);
                res.status(200).json({ success: true, token, email, name });
            }
        }
    } catch (e) {
        res.status(401).json({ message: 'login fail' });
    }
});

module.exports = app;
