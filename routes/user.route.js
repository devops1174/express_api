const router = require('express').Router();
const { userService } = require("../services/user.service");
const { body, param, query } = require('express-validator');
const { inputValidate, upload, smtpTransport } = require('../helpers/util');
const gm = require('gm');
const excel = require('excel4node');

/**
 * @swagger
 * /user:
 *    get:
 *      description: list all user
 *      tags:
 *        - user
 *      responses:
 *          200: 
 *            description: list success
 *      parameters:
 *           - in: query
 *             name: username
 *             type: string
 *             description: user id
 *             required: true
 *             default: 00001
 */
router.get("/", async (req, res) => {
    try {
        const resp = await userService.findAll();
        res.status(200).json({
            success: true,
            data: resp
        });
    } catch (e) {
        res.status(200).json({ success: false, error: e.message });
    }
});

const postValidator = [
    body("name").notEmpty(),
    body("email").isEmail(),
    body("password").notEmpty().isLength({ min: 8 }),
    inputValidate,
];

router.post("/", ...postValidator, async (req, res) => {
    try {
        const { name, email, password } = req.body;
        const resp = await userService.creatUser(name, email, password);
        res.status(201).json({
            success: true,
            data: resp
        });
    } catch (e) {
        res.status(200).json({ success: false, error: e.message });
    }
});

const delValidator = [
    param('id').notEmpty().isInt(),
    inputValidate
]

router.delete("/:id", ...delValidator, async (req, res) => {
    try {
        const { id } = req.params;
        const resp = await userService.delUser(id);
        res.status(200).json({
            success: true,
            data: resp
        });
    } catch (e) {
        res.status(200).json({ success: false, error: e.message });
    }
});

const putValidator = [
    param("id").notEmpty().isInt(),
    body("name").notEmpty(),
    body("password").notEmpty().isLength({ min: 8 }),
    inputValidate,
];

router.put('/:id', ...putValidator, async (req, res) => {
    try {
        const { id } = req.params;
        const { name, password } = req.body;
        const resp = userService.updateUser(id, name, password);
        res.status(200).json({ success: true, data: resp });
    } catch (e) {
        res.status(200).json({ success: false, error: e.message });
    }
})

const getPageValidator = [
    query('limit').notEmpty().isInt({ min: 5, max: 100 }),
    query('offset').notEmpty().isInt({ min: 0 }),
    inputValidate
];

router.get('/page', ...getPageValidator, async (req, res) => {
    try {
        const { offset, limit } = req.query;
        const { resp, total } = await userService.findByPage(offset, limit);
        res.status(200).json({ success: true, data: resp, total });
    } catch (e) {
        res.status(200).json({ success: false, error: e.message });
    }
});

const getByIdValidator = [
    query('id').notEmpty().isInt({ min: 0 }),
    inputValidate
];
router.get('/by-id', ...getByIdValidator, async (req, res) => {
    try {
        const { id } = req.query;
        const resp = await userService.findById(id);
        res.status(200).json({ success: true, data: resp });
    } catch (e) {
        res.status(200).json({ success: false, error: e.message });
    }
})

const getByEmailValidator = [
    query('email').isEmail(),
    inputValidate
];
router.get('/by-email', ...getByEmailValidator, async (req, res) => {
    try {
        const { email } = req.query;
        const resp = await userService.findByEmail(email);
        if (resp) {
            res.status(200).json({ success: true, data: resp });
        } else {
            res.status(200).json({ success: false, error: `${email} not found !!` });
        }
    } catch (e) {
        res.status(200).json({ success: false, error: e.message });
    }
});

router.post('/avatar', upload.single('avatar'), (req, res) => {
    res.status(200).json({ success: true, message: "upload success" });
});

router.get('/resize', (req, res) => {
    gm('/app/express_api/uploads/logo.png').resize(50, 50).write('/app/express_api/uploads/resize.png', (err) => {
        if (!err) {
            res.status(200).json({ success: true, message: "resize success" });
        }
        else {
            res.status(200).json({ success: false, message: err });
        }
    });
});

router.get('/download', (req, res) => {
    const file = '/app/express_api/uploads/resize.png';
    res.download(file, 'img.png');
});

router.post('/send-email', async (req, res) => {
    try {
        const msg = {
            to: '',
            subject: '',
            html: ''
        }
        const sendResp = await smtpTransport.sendMail({ ...msg });
        res.status(200).json({ success: true, message: 'send email success' });
    } catch (e) {
        res.status(200).json({ success: false, message: "send email error" });
    }
});

router.get('/export', async (req, res) => {
    try {
        const excelName = "/app/express_api/uploads/text.xlsx";
        const wb = new excel.Workbook();
        const sh = wb.addWorksheet("Sheet 1");
        const resp = await userService.findAll();
        sh.cell(1, 1).string("Email");
        sh.cell(1, 2).string("Name");
        let i = 1;
        resp.map((model) => {
            i++;
            sh.cell(i, 1).string(model.email);
            sh.cell(i, 2).string(model.name);
        });

        wb.write(excelName, (error) => {
            if (error) {
                res.status(200).json({ success: false, message: "can not export excel" });
            } else {
                res.download(excelName, 'export.xlsx')
            }
        });
    } catch (e) {
        res.status(200).json({ success: false, message: "can not export excel" });
    }
});

module.exports = router;